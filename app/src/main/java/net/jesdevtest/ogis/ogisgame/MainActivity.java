package net.jesdevtest.ogis.ogisgame;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.renderscript.Float2;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import net.jesdevtest.ogisgame.items.MapResourceItem;
import net.jesdevtest.ogisgame.items.UserTappableItem;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.logging.Logger;

import static java.lang.String.format;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_LOCATION_CODE = 777;
    private static final float STEP_TO_METERS = 0.804672f;
    private static final long minTime = 1 * 3 * 1000; // 3 seconds
    private static final long minDistance = 0;
    private static final int maxDelay = 1 * 3 * 1000; // 3 seconds
    private static final long SPAWN_RANGE = 5l;

    private List<MapResourceItem> mapItems;

    private MapView map;
    private Marker playerItem;

    private TextView mTextMessage;


    private double[] oldPosition = new double[2];
    private long currentSteps = -1;
    private long initialSteps = -1;

    private LocationListener locationListener;
    private LocationManager locationManager;

    private SensorEventListener sensorEventListener;
    private SensorManager sensorManager;

    private String bestProvider;

    private float distanceTraveled = 0.0f;
    private float resourceHit = 0.0f;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            mTextMessage = findViewById(R.id.message);
            ListView resourceList = findViewById(R.id.resource_list);
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setVisibility(View.INVISIBLE);
                    resourceList.setVisibility(View.INVISIBLE);
                    mTextMessage.setText(R.string.title_home);
                    map.setVisibility(View.VISIBLE);
                    return true;
                case R.id.navigation_dashboard:
                    map.setVisibility(View.INVISIBLE);
                    //mTextMessage.setText(R.string.title_dashboard);
                    //mTextMessage.setVisibility(View.VISIBLE);
                    resourceList.setVisibility(View.VISIBLE);
                    getResourceList();
                    return true;
                case R.id.navigation_notifications:
                    map.setVisibility(View.INVISIBLE);
                    resourceList.setVisibility(View.INVISIBLE);
                    //mTextMessage.setText(R.string.title_notifications);
                    //mTextMessage.setVisibility(View.VISIBLE);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        setContentView(R.layout.activity_main);

        mapItems = new ArrayList<>();
        map = findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(false);
        map.setMultiTouchControls(true);

        updatePlayerItem(new GeoPoint(0.0,0.0));

        final IMapController mapController = map.getController();
        mapController.setZoom(18.0);

        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        getLocationProvider();

        locationListener = new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Logger.getLogger(MainActivity.class.getName()).info(provider+" Status changes to "+status);
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
                Snackbar.make(findViewById(R.id.container), R.string.no_permissions, Snackbar.LENGTH_INDEFINITE)
                        .show();
            }

            @Override
            public void onLocationChanged(Location location) {
                // Do work with new location. Implementation of this method will be covered later.
                updateUserLocation(location);
            }
        };



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_CODE);
        }
        requestLocationUpdates();

        requestStepCounter();

    }

    public void onResume() {
        super.onResume();

        map.onResume();

        getLocationProvider();

        requestLocationUpdates();
        requestSteps();
    }

    public void onPause() {
        super.onPause();

        map.onPause();

        locationManager.removeUpdates(locationListener);
        if(currentSteps>=0) {
            sensorManager.unregisterListener(sensorEventListener);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_CODE:
                if(grantResults.length >0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestLocationUpdates();
                } else {
                    Snackbar.make(findViewById(R.id.container), R.string.no_permissions, Snackbar.LENGTH_INDEFINITE)
                            .show();
                }
        }
    }

    @SuppressLint("MissingPermission")
    private void updateUserLocation(Location location) {
        Logger.getLogger(MainActivity.class.getName()).info((location!=null?"Received a location "+location.toString():"load map!"));
        final IMapController mapController = map.getController();
        Location lastLocation = location;
        if (lastLocation == null ) {
            lastLocation = locationManager.getLastKnownLocation(bestProvider);
        }
        if(lastLocation!=null) {
            final GeoPoint point = new GeoPoint(lastLocation);
            mapController.setCenter(point);
            updatePlayerItem(point);
        }
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdates() {
        Logger.getLogger(MainActivity.class.getName()).info("Requesting locations.");
        locationManager.requestLocationUpdates(bestProvider, minTime, minDistance, locationListener);
        updateUserLocation(null);
    }

    private void getLocationProvider() {
        Criteria criteria = new Criteria();
        bestProvider = locationManager.getBestProvider(criteria,true);
        Logger.getLogger(MainActivity.class.getName()).info("Best provider set to "+bestProvider);
    }

    private void updatePlayerItem(GeoPoint point) {
        if (playerItem == null) {
            //initialize the player item
            playerItem = new Marker(map);
            playerItem.setAnchor(Marker.ANCHOR_CENTER,Marker.ANCHOR_BOTTOM);
            oldPosition = new double[]{point.getLatitude(), point.getLongitude()};
        }
        playerItem.setPosition(point);
        oldPosition = new double[]{point.getLatitude(), point.getLongitude()};
        map.getOverlays().add(playerItem);
        map.invalidate();
        calcPlayerDelta();
    }

    private void requestStepCounter() {
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        PackageManager packageManager = getPackageManager();
        if(currentApiVersion >= android.os.Build.VERSION_CODES.KITKAT
                && packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER)
                && packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_DETECTOR)) {
            //if we have this feature, we should track steps to enhance accuracy or try and counter the location being
            //denied.
            currentSteps = 0;
            sensorManager = (SensorManager) getSystemService(Activity.SENSOR_SERVICE);
            sensorEventListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent event) {
                    Logger.getLogger(MainActivity.class.getName()).info("User Step event!");
                    if (initialSteps < 1) {
                        initialSteps = (int) event.values[0];
                    }

                    currentSteps = (int) event.values[0] - initialSteps;
                    calcPlayerDelta();
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int accuracy) {
                    //no-op
                }
            };

            requestSteps();
        }
    }

    private void requestSteps() {
        if(currentSteps<0){
            return;
        }
        Sensor sensor;
        sensor = Objects.requireNonNull(sensorManager).getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        sensorManager.registerListener(sensorEventListener, sensor, SensorManager.SENSOR_DELAY_FASTEST, maxDelay);
    }

    private void calcPlayerDelta() {
        final GeoPoint point = playerItem.getPosition();
        float[] results = new float[1];
        float stepDistance;
        Location.distanceBetween(oldPosition[0],oldPosition[1],point.getLatitude(),point.getLongitude(),results);
        //compare to steps
        //10000 steps ~ 8046.72 meters
        stepDistance = currentSteps * STEP_TO_METERS;
        Logger.getLogger(MainActivity.class.getName()).info("Step distance "+stepDistance);
        Logger.getLogger(MainActivity.class.getName()).info("GPS distance "+results[0]);
        //take the greater of the two distances
        if(stepDistance > results[0]) {
            distanceTraveled = stepDistance;
        } else {
            distanceTraveled = results[0];
        }

        playerItem.setTitle(format(getString(R.string.player_title), distanceTraveled));
        checkForResources();
    }

    private void checkForResources() {
        //See if we should pop up a resource near the player
        //The longer a player goes the on a session, the higher the chance
        //Every km the user should have a good chance to get a resource
        //TODO: add leveling maybe?

        final float distanceMod = (distanceTraveled/(2000.0f * (1.0f + resourceHit)));
        final Random random = new Random();
        final float chance = random.nextFloat() + distanceMod;
        Logger.getLogger(MainActivity.class.getName()).info("Chance: "+chance);
        if(chance>=1.0f) {
            Logger.getLogger(MainActivity.class.getName()).info("Resource reward!");
            resourceHit += 1.0f;
            spawnResource();
        }
    }

    private void spawnResource() {
        MapResourceItem resource = new MapResourceItem("Resource", "A resource pack!",
                randomPointNearPoint(playerItem.getPosition(), SPAWN_RANGE), playerItem,
                map, getResources().getDrawable(R.drawable.ic_menu_compass));
        map.getOverlays().add(resource);
        map.invalidate();
        mapItems.add(resource);
    }

    private GeoPoint randomPointNearPoint(GeoPoint point, long range) {
        Random random = new Random();
        double radius= range / 111000.0f;
        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radius * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);
        double new_x = x / Math.cos(Math.toRadians(point.getLatitude()));
        double randLon = new_x + point.getLongitude();
        double randLat = y + point.getLatitude();
        return new GeoPoint(randLat, randLon);

    }

    private void getResourceList() {
        ListView resourceList = findViewById(R.id.resource_list);
        resourceList.setAdapter(new ArrayAdapter<>(getApplicationContext(), R.layout.resource_list_item, mapItems));
    }
}
